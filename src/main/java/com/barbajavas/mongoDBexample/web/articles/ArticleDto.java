package com.barbajavas.mongoDBexample.web.articles;

public record ArticleDto(String title, String content, String summary, String author) {
}
