package com.barbajavas.mongoDBexample.web.articles;

import com.barbajavas.mongoDBexample.application.ArticleManager;
import com.barbajavas.mongoDBexample.models.article.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
@CrossOrigin(origins = "*")
public class ArticleController {
    private final ArticleManager articleManager;

    @Autowired
    public ArticleController(ArticleManager articleManager) {
        this.articleManager = articleManager;
    }

    @PostMapping("/articles")
    ResponseEntity<Article> createNewArticle(@RequestBody ArticleDto articleDto) {
        return ResponseEntity.ok(articleManager.createNewArticle(
                articleDto.title(), articleDto.content(), articleDto.summary(), articleDto.author())
        );
    }

    @GetMapping("articles")
    ResponseEntity<List<Article>> getArticles() {
        return ResponseEntity.ok(articleManager.getArticles());
    }

    @DeleteMapping("articles/{tid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteArticle(@PathVariable String tid) {
        articleManager.removeArticle(tid);
    }
}