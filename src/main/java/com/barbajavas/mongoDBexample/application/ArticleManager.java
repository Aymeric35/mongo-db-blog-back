package com.barbajavas.mongoDBexample.application;

import com.barbajavas.mongoDBexample.models.article.Article;
import com.barbajavas.mongoDBexample.models.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ArticleManager {
    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleManager(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Article createNewArticle(String title, String content, String summary, String author) {
        Article article = new Article(title, content, summary, UUID.randomUUID().toString(), author);
        articleRepository.save(article);
        return article;
    }

    public List<Article> getArticles() {
        return articleRepository.findAll();
    }

    public Optional<Article> findById(String tid) {
        return articleRepository.findById(tid);
    }

    public void removeArticle(String tid) {
        articleRepository.deleteById(tid);
    }
}
