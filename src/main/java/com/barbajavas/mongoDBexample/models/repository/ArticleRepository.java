package com.barbajavas.mongoDBexample.models.repository;

import com.barbajavas.mongoDBexample.models.article.Article;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ArticleRepository extends MongoRepository<Article, String> {
}
