package com.barbajavas.mongoDBexample.models.article;

public enum ArticleState {
    PUBLISHED, DRAFT
}
