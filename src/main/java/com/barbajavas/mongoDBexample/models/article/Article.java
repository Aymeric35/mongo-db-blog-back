package com.barbajavas.mongoDBexample.models.article;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Getter
@NoArgsConstructor
public class Article {
    @Id
    private String tid;
    private String title;
    private String content;
    private String summary;
    private String author;
    private ArticleState articleState;

    public Article(String title, String content, String summary, String tid, String author) {
        this.title = title;
        this.content = content;
        this.summary = summary;
        this.tid = tid;
        this.author = author;
        this.articleState = ArticleState.DRAFT;
    }
}
